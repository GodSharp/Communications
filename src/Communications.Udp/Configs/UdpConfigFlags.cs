﻿namespace GodSharp.Communications.Udp
{
    /// <summary>
    /// The type of udp socket config.
    /// </summary>
    public enum UdpConfigFlags
    {
        /// <summary>
        /// The client
        /// </summary>
        Client = 0,
        /// <summary>
        /// The server
        /// </summary>
        Server = 1,
        /// <summary>
        /// The server client
        /// </summary>
        ServerClient = 2
    }
}