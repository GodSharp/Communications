﻿namespace GodSharp.Communications.Udp
{
    /// <summary>
    ///  The type of udp socket protocol.
    /// </summary>
    public enum UdpTypeFlags
    {
        /// <summary>
        /// The client
        /// </summary>
        Client = 0,
        /// <summary>
        /// The server
        /// </summary>
        Server = 1
    }
}