﻿using GodSharp.Communications.Abstractions;
using System;

namespace GodSharp.Communications.Udp
{
    /// <summary>
    /// 
    /// </summary>
    public static class ICommunicationServiceExtension
    {
        /// <summary>
        /// Adds the provider.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="tcpCommunicationProviderOptionsAction">The TCP Communication provider options action.</param>
        /// <returns></returns>
        public static void AddUdpProvider(this ICommunicationService service, Action<UdpCommunicationProviderOptions> tcpCommunicationProviderOptionsAction)
        {
            UdpCommunicationProviderOptions parameter = new UdpCommunicationProviderOptions();

            tcpCommunicationProviderOptionsAction?.Invoke(parameter);

            service.AddProvider(new UdpCommunicationProvider(parameter));
        }
    }
}
