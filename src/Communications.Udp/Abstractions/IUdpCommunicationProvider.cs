﻿using GodSharp.Communications.Abstractions;
using GodSharp.Sockets;

namespace GodSharp.Communications.Udp
{
    /// <summary>
    /// IUdpCommunicationProvider
    /// </summary>
    /// <seealso cref="GodSharp.Communications.Abstractions.ICommunicationProvider" />
    public interface IUdpCommunicationProvider: ICommunicationProvider
    {
        /// <summary>
        /// Gets the Udp sender.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        UdpSender GetUdpSender(int id);
    }
}
