﻿using GodSharp.Communications.Abstractions;
using GodSharp.Opc;

namespace GodSharp.Communications.OpcDa
{
    /// <summary>
    /// OpcDaCommunicationHandler interface.
    /// </summary>
    /// <seealso cref="GodSharp.Communications.Abstractions.ICommunicationHandler" />
    public interface IOpcDaCommunicationHandler : ICommunicationHandler
    {
        /// <summary>
        /// Called when [Tcp data handler].
        /// </summary>
        /// <param name="destination">The destination.</param>
        /// <param name="item">The item.</param>
        /// <param name="client">The client.</param>
        void OnOpcDaDataHandler(string destination, OpcTagItem item, OpcDaClient client);
    }
}