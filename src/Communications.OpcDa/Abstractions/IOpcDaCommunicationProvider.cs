﻿using GodSharp.Communications.Abstractions;
using GodSharp.Opc;

namespace GodSharp.Communications.OpcDa
{
    /// <summary>
    /// OpcDaCommunicationProvider interface.
    /// </summary>
    /// <seealso cref="GodSharp.Communications.Abstractions.ICommunicationProvider" />
    public interface IOpcDaCommunicationProvider : ICommunicationProvider
    {
        /// <summary>
        /// Gets the opc client.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        OpcDaClient GetOpcDaClient(int id);
    }
}