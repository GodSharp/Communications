﻿namespace GodSharp.Communications.OpcDa
{
    /// <summary>
    /// OpcDa Server Config
    /// </summary>
    public class OpcDaServerConfig
    {
        /// <summary>
        /// Gets or sets the prog identifier.
        /// </summary>
        /// <value>
        /// The prog identifier.
        /// </value>
        public string ProgId { get; set; }

        /// <summary>
        /// Gets or sets the host.
        /// </summary>
        /// <value>
        /// The host.
        /// </value>
        public string Host { get; set; }

        /// <summary>
        /// Gets or sets the name of the group.
        /// </summary>
        /// <value>
        /// The name of the group.
        /// </value>
        public string GroupName { get; set; }

        /// <summary>
        /// Gets or sets the default update rate.
        /// </summary>
        /// <value>
        /// The default update rate.
        /// </value>
        public int DefaultUpdateRate { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpcDaServerConfig"/> class.
        /// </summary>
        /// <param name="progId">The prog identifier.</param>
        /// <param name="host">The host.</param>
        /// <param name="groupName">Name of the group.</param>
        /// <param name="updateRate">The update rate.</param>
        public OpcDaServerConfig(string progId, string host = "127.0.0.1", string groupName = "OpcTagGroup", int updateRate = 300)
        {
            ProgId = progId;
            Host = host;
            GroupName = groupName;
            DefaultUpdateRate = updateRate;
        }
    }
}
