﻿using GodSharp.Communications.Abstractions;
using System;

namespace GodSharp.Communications.OpcDa
{
    /// <summary>
    /// OpcDaTagItem
    /// </summary>
    public class OpcDaTagConfig : ConfigBase
    {
        /// <summary>
        /// Gets or sets the item identifier.
        /// </summary>
        /// <value>
        /// The item identifier.
        /// </value>
        public string ItemId { get; set; }

        /// <summary>
        /// Gets or sets the name of the group.
        /// </summary>
        /// <value>
        /// The name of the group.
        /// </value>
        public string GroupName { get; set; }

        /// <summary>
        /// Gets or sets the type of the data.
        /// </summary>
        /// <value>
        /// The type of the data.
        /// </value>
        public string DataType { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets the default value.
        /// </summary>
        /// <value>
        /// The default value.
        /// </value>
        public object DefaultValue { get; set; }

        /// <summary>
        /// Gets or sets the time stamp.
        /// </summary>
        /// <value>
        /// The time stamp.
        /// </value>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Gets or sets the quality.
        /// </summary>
        /// <value>
        /// The quality.
        /// </value>
        public int Quality { get; set; }

        /// <summary>
        /// Gets or sets the client handle.
        /// </summary>
        /// <value>
        /// The client handle.
        /// </value>
        public int ClientHandle { get; set; }

        /// <summary>
        /// Gets or sets the server handle.
        /// </summary>
        /// <value>
        /// The server handle.
        /// </value>
        public int ServerHandle { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpcDaTagConfig"/> class.
        /// </summary>
        public OpcDaTagConfig() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpcDaTagConfig"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="itemId">The item identifier.</param>
        public OpcDaTagConfig(int id, string itemId)
        {
            Id = id;

            if (itemId.IsNullOrWhiteSpace()) throw new ArgumentNullException(nameof(itemId));
            ItemId = itemId;
        }
    }
}
