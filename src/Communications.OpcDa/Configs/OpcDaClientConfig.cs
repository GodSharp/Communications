﻿using System.Collections.Generic;

namespace GodSharp.Communications.OpcDa
{
    /// <summary>
    /// The config of opcda protocol.
    /// </summary>
    public class OpcDaClientConfig
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the server.
        /// </summary>
        /// <value>
        /// The server.
        /// </value>
        public OpcDaServerConfig Server { get; set; }

        /// <summary>
        /// Gets or sets the tags.
        /// </summary>
        /// <value>
        /// The tags.
        /// </value>
        public IList<OpcDaTagConfig> Tags { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpcDaClientConfig"/> class.
        /// </summary>
        public OpcDaClientConfig()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpcDaClientConfig"/> class.
        /// </summary>
        /// <param name="server">The server.</param>
        /// <param name="tags">The tags.</param>
        public OpcDaClientConfig(OpcDaServerConfig server, IList<OpcDaTagConfig> tags)
        {
            Server = server;
            Tags = tags;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpcDaClientConfig"/> class.
        /// </summary>
        /// <param name="progId">The prog identifier.</param>
        /// <param name="host">The host.</param>
        /// <param name="tags">The tags.</param>
        public OpcDaClientConfig(string progId,string host, IList<OpcDaTagConfig> tags)
        {
            if (progId.IsNullOrWhiteSpace()) throw new System.ArgumentNullException(nameof(progId));
            if (host.IsNullOrWhiteSpace()) throw new System.ArgumentNullException(nameof(host));

            Server = new OpcDaServerConfig(progId, host);
            Tags = tags;
        }
    }
}
