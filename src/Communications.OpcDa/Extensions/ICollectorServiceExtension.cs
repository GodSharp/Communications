﻿using GodSharp.Communications.Abstractions;
using System;

namespace GodSharp.Communications.OpcDa
{
    /// <summary>
    /// 
    /// </summary>
    public static class ICommunicationServiceExtension
    {
        /// <summary>
        /// Uses the opc da provider.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="optionsAction">The options action.</param>
        public static void UseOpcDaProvider(this ICommunicationManager service, Action<OpcDaCommunicationProviderOptions> optionsAction)
        {
            OpcDaCommunicationProviderOptions parameter = new OpcDaCommunicationProviderOptions();

            optionsAction?.Invoke(parameter);

            UseOpcDaProvider(service, parameter);
        }

        /// <summary>
        /// Uses the opc da provider.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="options">The options.</param>
        public static void UseOpcDaProvider(this ICommunicationManager service, OpcDaCommunicationProviderOptions options)
        {
            service.AddProvider(new OpcDaCommunicationProvider(options));
        }
    }
}
