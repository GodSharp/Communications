﻿using GodSharp.Communications.Abstractions;
using System.Collections.Generic;

namespace GodSharp.Communications.OpcDa
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="GodSharp.Communications.Abstractions.CommunicationProviderOptionsBase" />
    public class OpcDaCommunicationProviderOptions : CommunicationProviderOptionsBase
    {
        /// <summary>
        /// Gets or sets the clients.
        /// </summary>
        /// <value>
        /// The clients.
        /// </value>
        public IList<OpcDaClientConfig> Clients { get; set; }

        /// <summary>
        /// Gets or sets the shutdown event handler.
        /// </summary>
        /// <value>
        /// The shutdown event handler.
        /// </value>
        public OpcDaCommunicationShutdownEventHandler ShutdownEventHandler { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpcDaCommunicationProviderOptions"/> class.
        /// </summary>
        public OpcDaCommunicationProviderOptions()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpcDaCommunicationProviderOptions"/> class.
        /// </summary>
        /// <param name="clients">The clients.</param>
        /// <param name="shutdown">The shutdown.</param>
        public OpcDaCommunicationProviderOptions(IList<OpcDaClientConfig> clients, OpcDaCommunicationShutdownEventHandler shutdown)
        {
            Clients = clients;
            ShutdownEventHandler = shutdown;
        }
    }
}