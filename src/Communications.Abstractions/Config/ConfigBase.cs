﻿namespace GodSharp.Communications.Abstractions
{
    /// <summary>
    /// The config base class.
    /// </summary>
    public class ConfigBase
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ConfigBase"/> is available.
        /// </summary>
        /// <value>
        ///   <c>true</c> if available; otherwise, <c>false</c>.
        /// </value>
        public bool Available { get; set; }

        /// <summary>
        /// Gets or sets the misc.
        /// </summary>
        /// <value>
        /// The misc.
        /// </value>
        public string Misc { get; set; }

        /// <summary>
        /// Gets or sets the entry point.
        /// </summary>
        /// <value>
        /// The entry point.
        /// </value>
        public string EntryPoint { get; set; }
    }
}
