﻿namespace GodSharp.Communications
{
    public delegate void EventHandler<in TEventArgs>(TEventArgs args);// where TEventArgs : EventArgs;
}