﻿using System.Reflection;

namespace GodSharp.Communications.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICommunicationManager
    {
        /// <summary>
        /// Adds the provider.
        /// </summary>
        /// <param name="provider">The provider.</param>
        void AddProvider(ICommunicationProvider provider);

        /// <summary>
        /// Gets the provider.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <returns></returns>
        ICommunicationProvider GetProvider(string providerName);

        /// <summary>
        /// Starts this instance.
        /// </summary>
        void Start();

        /// <summary>
        /// Restarts this instance.
        /// </summary>
        void Restart();

        /// <summary>
        /// Stops this instance.
        /// </summary>
        void Stop();

        /// <summary>
        /// Registers the container.
        /// </summary>
        /// <param name="container">The container.</param>
        void RegisterContainer(IModuleContainer container);

        /// <summary>
        /// Registers the container.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        void RegisterContainer<T>() where T : IModuleContainer, new();

        /// <summary>
        /// Builders this instance.
        /// </summary>
        ICommunicationManager Builder(params Assembly[] assemblies);
    }
}
