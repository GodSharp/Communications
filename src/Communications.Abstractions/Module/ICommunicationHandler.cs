﻿namespace GodSharp.Communications.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICommunicationHandler
    {
        /// <summary>
        /// Gets the entry points.
        /// </summary>
        /// <value>
        /// The entry points.
        /// </value>
        string[] EntryPoints { get; }
    }
}
