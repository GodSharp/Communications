﻿using System;
using System.Collections.Generic;

namespace GodSharp.Communications.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public static class ModuleManager
    {
        private static IModuleContainer container { get; set; }

        /// <summary>
        /// Register the container.
        /// </summary>
        /// <param name="container">The container.</param>
        public static void RegisterContainer(IModuleContainer container)
        {
            ModuleManager.container = container;
        }

        /// <summary>
        /// Registers the container.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static void RegisterContainer<T>() where T : IModuleContainer, new()
        {
            container = new T();
        }

        /// <summary>
        /// Adds the module.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <exception cref="NotImplementedException"></exception>
        public static void AddModule(Type type)
        {
            if (container == null) throw new NullReferenceException();

            container.AddModule(type);
        }

        /// <summary>
        /// Adds the module.
        /// </summary>
        /// <param name="types">The types.</param>
        /// <exception cref="NotImplementedException"></exception>
        public static void AddModule(ICollection<Type> types)
        {
            if (container == null) throw new NullReferenceException();

            container.AddModule(types);
        }

        /// <summary>
        /// Gets the module.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public static T GetModule<T>(Type type)
        {
            if (container == null) throw new NullReferenceException();

            return container.GetModule<T>(type);
        }
    }
}
