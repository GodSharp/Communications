﻿using System;
using System.Collections.Generic;

namespace GodSharp.Communications.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICommunicationProvider
    {
        /// <summary>
        /// Gets the name of the provider.
        /// </summary>
        /// <value>
        /// The name of the provider.
        /// </value>
        string ProviderName { get; }

        /// <summary>
        /// Gets the provider description.
        /// </summary>
        /// <value>
        /// The provider description.
        /// </value>
        string ProviderDescription { get; }

        /// <summary>
        /// Starts the specified ids.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        bool Start(params int[] ids);

        /// <summary>
        /// Restarts the specified ids.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        bool Restart(params int[] ids);

        /// <summary>
        /// Stops the specified ids.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        bool Stop(params int[] ids);

        /// <summary>
        /// Registers the modules.
        /// </summary>
        /// <param name="types">The types.</param>
        void RegisterModules(Type[] types);

        /// <summary>
        /// Healthes this instance.
        /// </summary>
        /// <returns></returns>
        CommunicationModuleStateFlags Health();

        /// <summary>
        /// Healthes the specified ids.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        IDictionary<int, CommunicationModuleStateFlags> Health(params int[] ids);
    }
}
