﻿using System;

namespace GodSharp.Communications.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.All)]
    public class NameDescriptionAttribute : Attribute
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NameDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public NameDescriptionAttribute(string name) : this(name, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NameDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        public NameDescriptionAttribute(string name,string description)
        {
            Name = name;
            Description = description;
        }
    }
}
