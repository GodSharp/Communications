﻿using System;

namespace GodSharp.Communications.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class EntryPointAttribute : Attribute
    {
        /// <summary>
        /// Gets the entry point.
        /// </summary>
        /// <value>
        /// The entry point.
        /// </value>
        public string EntryPoint { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntryPointAttribute"/> class.
        /// </summary>
        /// <param name="entry">The points.</param>
        public EntryPointAttribute(string entry)
        {
            EntryPoint = entry;
        }
    }
}
