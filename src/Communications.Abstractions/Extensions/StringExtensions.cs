﻿namespace GodSharp.Communications
{
    /// <summary>
    /// 
    /// </summary>
    public static class StringExtensions
    {
        //[assembly: InternalsVisibleTo("")]

        /// <summary>
        /// Determines whether [is null or white space].
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if [is null or white space] [the specified value]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNullOrWhiteSpace(this string value)
        {
#if NET35
            return string.IsNullOrEmpty(value) || value == "";
#else
            return string.IsNullOrWhiteSpace(value);
#endif
        }
    }
}
