﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GodSharp.Communications.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public static class AssemblyExtensions
    {
        /// <summary>
        /// Gets the defined types.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        public static IEnumerable<Type> GetDefinedTypes(this System.Reflection.Assembly assembly, Func<Type, bool> predicate = null)
        {
            IEnumerable<Type> results = null;

#if NET35 || NET40
            results = assembly.GetTypes();
#else
            results = assembly.DefinedTypes.Select(t => t.AsType());
#endif
            if (predicate != null) results = results?.Where(predicate);

            return results;
        }
    }
}
