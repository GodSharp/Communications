﻿namespace GodSharp.Communications.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public enum CommunicationModuleStateFlags
    {
        /// <summary>
        /// The none
        /// </summary>
        None = 0,
        /// <summary>
        /// The initialized
        /// </summary>
        Initialized = 1,
        /// <summary>
        /// The stopped
        /// </summary>
        Stopped = 2,
        /// <summary>
        /// The stopping
        /// </summary>
        Stopping = 3,
        /// <summary>
        /// The starting
        /// </summary>
        Starting = 4,
        /// <summary>
        /// The started
        /// </summary>
        Started = 5,
        /// <summary>
        /// The started with error
        /// </summary>
        StartWithError = 6,
        /// <summary>
        /// The with error
        /// </summary>
        StopWithError = 7
    }
}
