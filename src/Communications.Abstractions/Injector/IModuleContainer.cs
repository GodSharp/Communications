﻿using System;
using System.Collections.Generic;

namespace GodSharp.Communications.Abstractions
{
    /// <summary>
    /// 
    /// </summary>
    public interface IModuleContainer
    {
        /// <summary>
        /// Adds the module.
        /// </summary>
        /// <param name="type">The type.</param>
        void AddModule(Type type);

        /// <summary>
        /// Adds the module.
        /// </summary>
        /// <param name="types">The types.</param>
        void AddModule(ICollection<Type> types);

        /// <summary>
        /// Gets the module.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        object GetModule(Type type);

        /// <summary>
        /// Gets the module.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        T GetModule<T>(Type type);
    }
}
