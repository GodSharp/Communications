﻿using GodSharp.Communications.Abstractions;
using System;

namespace GodSharp.Communications.Tcp
{
    /// <summary>
    /// 
    /// </summary>
    public static class ICommunicationServiceExtension
    {
        /// <summary>
        /// Adds the provider.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="tcpCommunicationProviderOptionsAction">The TCP Communication provider options action.</param>
        /// <returns></returns>
        public static void AddTcpProvider(this ICommunicationService service, Action<TcpCommunicationProviderOptions> tcpCommunicationProviderOptionsAction)
        {
            TcpCommunicationProviderOptions parameter = new TcpCommunicationProviderOptions();

            tcpCommunicationProviderOptionsAction?.Invoke(parameter);

            service.AddProvider(new TcpCommunicationProvider(parameter));
        }
    }
}
