﻿using GodSharp.Communications.Abstractions;
using GodSharp.Sockets;
using System;
using System.Linq;

namespace GodSharp.Communications.Tcp
{
    /// <summary>
    /// 
    /// </summary>
    public static class ITcpCommunicationProviderExtension
    {
        /// <summary>
        /// Adds the provider.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="tcpCommunicationProviderOptionsAction">The TCP Communication provider options action.</param>
        /// <returns></returns>
        public static TcpSender GetTcpSender(this ITcpCommunicationProvider provider, int id, TcpConfigFlags socketType= TcpConfigFlags.TcpClient)
        {
            switch (socketType)
            {
                case TcpConfigFlags.TcpClient:
                    if (provider.TcpClients?.ContainsKey(id) == true) return provider.TcpClients[id].Sender;
                    break;
                case TcpConfigFlags.TcpServer:
                    if (provider.TcpServers?.ContainsKey(id) == true) return provider.TcpServers[id].Clients.FirstOrDefault();
                    break;
                default:
                    break;
            }

            return null;
        }
    }
}
