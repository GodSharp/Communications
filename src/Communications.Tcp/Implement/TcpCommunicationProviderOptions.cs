﻿using GodSharp.Communications.Abstractions;
using System.Collections.Generic;

namespace GodSharp.Communications.Tcp
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="GodSharp.Communications.Abstractions.CommunicationProviderOptionsBase" />
    public class TcpCommunicationProviderOptions: CommunicationProviderOptionsBase
    {
        /// <summary>
        /// Gets or sets the configs.
        /// </summary>
        /// <value>
        /// The configs.
        /// </value>
        public ICollection<TcpConfig> Configs { get; set; }
        
        /// <summary>
        /// Gets or sets the client connected handler.
        /// </summary>
        /// <value>
        /// The client connected handler.
        /// </value>
        public TcpCommunicationClientConnectedEventHandler ClientConnectedHandler { get; set; }

        /// <summary>
        /// Gets or sets the client disconnected handler.
        /// </summary>
        /// <value>
        /// The client disconnected handler.
        /// </value>
        public TcpCommunicationClientDisconnectedEventHandler ClientDisconnectedHandler { get; set; }
        
        /// <summary>
        /// Gets or sets the client connected handler.
        /// </summary>
        /// <value>
        /// The client connected handler.
        /// </value>
        public TcpCommunicationClientConnectedEventHandler ServerClientConnectedHandler { get; set; }

        /// <summary>
        /// Gets or sets the client disconnected handler.
        /// </summary>
        /// <value>
        /// The client disconnected handler.
        /// </value>
        public TcpCommunicationClientDisconnectedEventHandler ServerClientDisconnectedHandler { get; set; }

        /// <summary>
        /// Gets or sets the exception handler.
        /// </summary>
        /// <value>
        /// The exception handler.
        /// </value>
        public TcpCommunicationExceptionEventHandler ExceptionHandler { get; set; }
    }
}