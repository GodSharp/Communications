﻿using GodSharp.Communications.Abstractions;
using GodSharp.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace GodSharp.Communications.Tcp
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="destination"></param>
    /// <param name="buffer"></param>
    /// <param name="sender"></param>
    internal delegate void TcpCommunicationDataEventHandler(string destination, byte[] buffer, TcpSender sender);
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    public delegate void TcpCommunicationClientConnectedEventHandler(TcpSender sender);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    public delegate void TcpCommunicationClientDisconnectedEventHandler(TcpSender sender);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="exception"></param>
    public delegate void TcpCommunicationExceptionEventHandler(TcpSender sender,Exception exception);

#pragma warning disable CS1584 // XML comment has syntactically incorrect cref attribute
#pragma warning disable CS1658 // Warning is overriding an error
    /// <summary>
    /// TcpCommunicationProvider
    /// </summary>
    /// <seealso cref="GodSharp.Communications.Abstractions.CommunicationProviderBase{GodSharp.Communications.Tcp.TcpCommunicationProviderOptions}" />
    /// <seealso cref="GodSharp.Communications.Abstractions.ICommunicationProvider" />
    [NameDescription("TcpCommunicationProvider", "Tcp protocol for GodSharp.Communications")]
#pragma warning restore CS1658 // Warning is overriding an error
#pragma warning restore CS1584 // XML comment has syntactically incorrect cref attribute
    public sealed class TcpCommunicationProvider : CommunicationProviderBase<TcpCommunicationProviderOptions, ICommunicationTcpHandler>, ITcpCommunicationProvider
    {
        private Dictionary<string, TcpCommunicationDataEventHandler> OnData { get; set; }

        TcpCommunicationClientConnectedEventHandler OnServerClientConnected;
        TcpCommunicationClientDisconnectedEventHandler OnServerClientDisconnected;
        TcpCommunicationClientConnectedEventHandler OnClientConnected;
        TcpCommunicationClientDisconnectedEventHandler OnClientDisconnected;
        TcpCommunicationExceptionEventHandler OnException;

        Dictionary<int, SocketClient> clients = null;
        Dictionary<int, SocketServer> servers = null;
        TcpConfig[] sConfigs = null;
        TcpConfig[] scConfigs = null;
        TcpConfig[] cConfigs = null;

        Dictionary<int, TcpSender> senders = null;

        bool initialized = false;
        bool started = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpCommunicationProvider"/> class.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        public TcpCommunicationProvider(TcpCommunicationProviderOptions parameter)
        {
            Initialize(parameter);
        }

        /// <summary>
        /// Initializes the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">
        /// parameter
        /// or
        /// ServerClientConfigs
        /// </exception>
        /// <exception cref="InvalidOperationException">The provider is initialized</exception>
        /// <exception cref="ArgumentException">
        /// Configs
        /// or
        /// Configs
        /// </exception>
        private bool Initialize(TcpCommunicationProviderOptions parameter)
        {
            if (parameter == null) throw new ArgumentNullException(nameof(parameter));

            OnServerClientConnected = parameter.ServerClientConnectedHandler;
            OnServerClientDisconnected = parameter.ServerClientDisconnectedHandler;
            OnClientConnected = parameter.ClientConnectedHandler;
            OnClientDisconnected = parameter.ClientDisconnectedHandler;
            OnException = parameter.ExceptionHandler;

            parameter.Configs = parameter.Configs?.Where(x => x.Available)?.ToArray();

            if (parameter == null || parameter.Configs.Count < 1) throw new ArgumentNullException(nameof(parameter));

            if (initialized) throw new InvalidOperationException("The provider is initialized");

            if (parameter.Configs.GroupBy(x => x.Id).Any(x => x.ToArray().Length > 1)) throw new ArgumentException($"Duplicate 'Id' at {nameof(parameter.Configs)}");

            TcpConfig[] configs = parameter.Configs.Where(x => x.Available).ToArray();

            sConfigs = configs.Where(x => x.ConfigType == TcpConfigFlags.TcpServer).ToArray();

            if (sConfigs.GroupBy(x => x.Port).Any(x => x.ToArray().Length > 1)) throw new ArgumentException($"Duplicate 'Port' at {nameof(parameter.Configs)}");

            if (sConfigs?.Length > 0)
            {
                scConfigs = configs.Where(x => x.ConfigType == TcpConfigFlags.TcpServerClient).ToArray();

                if (scConfigs?.Length < 1) throw new ArgumentNullException(nameof(parameter.Configs));
            }

            cConfigs = configs.Where(x => x.ConfigType == TcpConfigFlags.TcpClient).ToArray();
            
            if (sConfigs?.Length > 0)
            {
                servers = new Dictionary<int, SocketServer>();

                foreach (var item in sConfigs)
                {
                    if (servers.ContainsKey(item.Id)) continue;

                    servers.Add(item.Id, new SocketServer(item.Port) { OnData = SocketServerDataHandler, OnConnected = SocketServerClientConnectedHandler, OnClosed = SocketServerClientDisconnectedHandler, OnException = SocketExceptionHandler });
                }
            }

            if (cConfigs?.Length > 0)
            {
                clients = new Dictionary<int, SocketClient>();

                foreach (var item in cConfigs)
                {
                    if (clients.ContainsKey(item.Id)) continue;

                    clients.Add(item.Id, new SocketClient(item.Host, item.Port) { OnData = SocketClientDataHandler, OnConnected = SocketClientConnectedHandler, OnClosed = SocketClientDisconnectedHandler, OnException = SocketExceptionHandler });
                }
            }

            initialized = true;

            return initialized;
        }

        /// <summary>
        /// Starts the specified objs.
        /// </summary>
        /// <param name="ids">The id array.</param>
        /// <returns></returns>
        public bool Start(params int[] ids)
        {
            CheckingState(true, false, true);
            
            bool error = false;

            List<int> sKeys = new List<int>();
            List<int> cKeys = new List<int>();

            if (ids?.Length > 0)
            {
                if (servers?.Count > 0) sKeys = servers.Keys.Where(x => ids.Contains(x)).ToList();
                if (clients?.Count > 0) cKeys = clients.Keys.Where(x => ids.Contains(x)).ToList();
            }
            else
            {
                sKeys = servers?.Keys.ToList();
                cKeys = clients?.Keys.ToList();
            }

            if (sKeys?.Count > 0)
            {
                foreach (var key in sKeys)
                {
                    try
                    {
                        servers[key].Listen(int.MaxValue);

                        servers[key].Start();

                        if (!servers[key].Running)
                        {
                            error = true;
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        error = true;
                        Console.WriteLine(ex.Message);
                    }
                }
            }

            if (cKeys?.Count > 0)
            {
                foreach (var key in cKeys)
                {
                    try
                    {
                        clients[key].Connect();

                        if (!clients[key].Connected)
                        {
                            error = true;
                            break;
                        }

                        clients[key].Start();
                    }
                    catch (Exception ex)
                    {
                        error = true;
                        Console.WriteLine(ex.Message);
                    }
                }
            }

            senders = new Dictionary<int, TcpSender>();

            return started;
        }

        /// <summary>
        /// Stops the specified objs.
        /// </summary>
        /// <param name="ids">The id array.</param>
        /// <returns></returns>
        public bool Stop(params int[] ids)
        {
            CheckingState(true, true);

            if (servers?.Count > 0)
            {
                List<string> keys = new List<string>();

                foreach (var item in servers)
                {
                    try
                    {
                        if (!item.Value.Running) continue;

                        item.Value.Stop();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }

            if (clients?.Count > 0)
            {
                List<string> keys = new List<string>();

                foreach (var item in clients)
                {
                    try
                    {
                        if (!item.Value.Connected) continue;

                        item.Value.Stop();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Res the start.
        /// </summary>
        /// <param name="ids">The id array.</param>
        /// <returns></returns>
        public bool Restart(params int[] ids)
        {
            CheckingState(true, true);

            Stop();

            Start();

            return true;
        }

        /// <summary>
        /// Healthes this instance.
        /// </summary>
        /// <returns></returns>
        public CommunicationModuleStateFlags Health()
        {
            CheckingState(true, true);

            return CommunicationModuleStateFlags.Starting;
        }

        /// <summary>
        /// Healthes the specified objs.
        /// </summary>
        /// <param name="ids">The id array.</param>
        /// <returns></returns>
        public CommunicationModuleStateFlags[] Health(params int[] ids)
        {
            CheckingState(true, true);

            return null;
        }

        /// <summary>
        /// Sockets the data handler.
        /// </summary>
        /// <param name="client">The client.</param>
        /// <param name="buffer">The buffer.</param>
        private void SocketServerDataHandler(TcpSender client, byte[] buffer)
        {
            try
            {
                Console.WriteLine($"Tcp:{client.Guid} Received from client {client.RemoteEndPoint}");

                IPEndPoint point = client.RemoteEndPoint as IPEndPoint;

                TcpConfig config = scConfigs.FirstOrDefault(x => x.Host == point.Address.ToString() && (x.Port == 0 || x.Port == point.Port));
                
                SocketDataHandler(config, client, buffer);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Sockets the data handler.
        /// </summary>
        /// <param name="client">The client.</param>
        /// <param name="buffer">The buffer.</param>
        private void SocketClientDataHandler(TcpSender client, byte[] buffer)
        {
            try
            {
                Console.WriteLine($"Tcp:{client.Guid} Received from server {client.RemoteEndPoint}");

                IPEndPoint point = client.RemoteEndPoint as IPEndPoint;

                TcpConfig config = cConfigs.FirstOrDefault(x => x.Host == point.Address.ToString() && x.Port == point.Port);

                SocketDataHandler(config, client, buffer);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Sockets the data handler.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="client">The client.</param>
        /// <param name="buffer">The buffer.</param>
        private void SocketDataHandler(TcpConfig config, TcpSender client, byte[] buffer)
        {
            if (config == null)
            {
                return;
            }

            OnData[config.EntryPoint]?.Invoke(config.EntryPoint, buffer, client);
        }

        /// <summary>
        /// Sockets the server client connected handler.
        /// </summary>
        /// <param name="client">The client.</param>
        private void SocketServerClientConnectedHandler(TcpSender client)
        {
            Console.WriteLine($"Tcp:{client.Guid} client->server[{client.RemoteEndPoint}] connected");

            TcpConfig config = GetSocketConfig(client, TcpTypeFlags.Server);

            if (config != null)
            {
                RemoveSender(config.Id);

                senders.Add(config.Id, client);
            }

            OnServerClientConnected?.Invoke(client);
        }

        /// <summary>
        /// Sockets the server client disconnected handler.
        /// </summary>
        /// <param name="client">The client.</param>
        private void SocketServerClientDisconnectedHandler(TcpSender client)
        {
            Console.WriteLine($"Tcp:{client.Guid} client->server[{client.RemoteEndPoint}] disconnected");

            TcpConfig config = GetSocketConfig(client, TcpTypeFlags.Server);

            if (config != null) RemoveSender(config.Id);

            OnServerClientDisconnected?.Invoke(client);
        }

        /// <summary>
        /// Sockets the client connected handler.
        /// </summary>
        /// <param name="client">The client.</param>
        private void SocketClientConnectedHandler(TcpSender client)
        {
            Console.WriteLine($"Tcp:{client.Guid} server->client[{client.RemoteEndPoint}] connected");

            TcpConfig config = GetSocketConfig(client, TcpTypeFlags.Client);

            if (config != null)
            {
                RemoveSender(config.Id);

                senders.Add(config.Id, client);
            }

            OnClientConnected?.Invoke(client);
        }

        /// <summary>
        /// Sockets the client disconnected handler.
        /// </summary>
        /// <param name="client">The client.</param>
        private void SocketClientDisconnectedHandler(TcpSender client)
        {
            Console.WriteLine($"Tcp:{client.Guid} server->client[{client.RemoteEndPoint}] disconnected");

            TcpConfig config = GetSocketConfig(client, TcpTypeFlags.Client);

            if (config != null) RemoveSender(config.Id);

            OnClientDisconnected(client);
        }

        /// <summary>
        /// Sockets the exception handler.
        /// </summary>
        /// <param name="client">The client.</param>
        /// <param name="exception">The exception.</param>
        private void SocketExceptionHandler(TcpSender client, Exception exception)
        {
            Console.WriteLine($"Tcp:{client.Guid} Exception:{exception.Message}");

            OnException?.Invoke(client, exception);
        }

        /// <summary>
        /// Gets the socket configuration.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="flags">The flags.</param>
        /// <returns></returns>
        private TcpConfig GetSocketConfig(TcpSender sender,TcpTypeFlags flags)
        {
            TcpConfig config = null;
            IPEndPoint point = sender.RemoteEndPoint as IPEndPoint;

            switch (flags)
            {
                case TcpTypeFlags.Client:
                    config = cConfigs.FirstOrDefault(x => x.Host == point.Address.ToString() && x.Port == point.Port);
                    break;
                case TcpTypeFlags.Server:
                    config = scConfigs.FirstOrDefault(x => x.Host == point.Address.ToString() && (x.Port == 0 || x.Port == point.Port));
                    break;
                default:
                    break;
            }

            return config;
        }

        /// <summary>
        /// Removes the sender.
        /// </summary>
        /// <param name="id">The identifier.</param>
        private void RemoveSender(int id)
        {
            if (senders.ContainsKey(id)) senders.Remove(id);
        }

        /// <summary>
        /// Checkings the state.
        /// </summary>
        /// <param name="initialized">if set to <c>true</c> [initialized].</param>
        /// <param name="started">if set to <c>true</c> [started].</param>
        /// <param name="restarting">if set to <c>true</c> [restarting].</param>
        /// <exception cref="System.InvalidOperationException">
        /// The module is not initialized
        /// or
        /// The module is not started
        /// or
        /// The module is running
        /// </exception>
        private void CheckingState(bool initialized = false, bool started = false, bool restarting = false)
        {
            if (initialized && !this.initialized) throw new InvalidOperationException("The provider is not initialized");

            if (started && !this.started) throw new InvalidOperationException("The provider is not started");

            if (restarting && this.started) throw new InvalidOperationException("The provider is running");
        }

        /// <summary>
        /// Registers the modules.
        /// </summary>
        /// <param name="types">The types.</param>
        public void RegisterModules(Type[] types)
        {
            OnRegisterModules(types, (entry, handler) =>
            {
                if (!this.OnData.ContainsKey(entry)) this.OnData.Add(entry, null);
                this.OnData[entry] += handler.OnTcpDataHandler;
            });
        }

        /// <summary>
        /// Gets the TCP sender.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public TcpSender GetTcpSender(int id)
        {
            if (senders.ContainsKey(id)) return senders[id];

            return null;
        }
    }
}