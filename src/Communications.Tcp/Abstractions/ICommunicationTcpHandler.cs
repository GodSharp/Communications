﻿using GodSharp.Communications.Abstractions;
using GodSharp.Sockets;

namespace GodSharp.Communications.Tcp
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICommunicationTcpHandler: ICommunicationHandler
    {
        /// <summary>
        /// Called when [TCP data handler].
        /// </summary>
        /// <param name="destination">The destination.</param>
        /// <param name="buffer">The buffer.</param>
        /// <param name="sender">The sender.</param>
        void OnTcpDataHandler(string destination, byte[] buffer, TcpSender sender);
    }
}
