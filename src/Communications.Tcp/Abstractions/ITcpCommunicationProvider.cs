﻿using GodSharp.Communications.Abstractions;
using GodSharp.Sockets;

namespace GodSharp.Communications.Tcp
{
    /// <summary>
    /// ITcpCommunicationProvider
    /// </summary>
    /// <seealso cref="GodSharp.Communications.Abstractions.ICommunicationProvider" />
    public interface ITcpCommunicationProvider: ICommunicationProvider
    {
        /// <summary>
        /// Gets the TCP sender.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        TcpSender GetTcpSender(int id);
    }
}
