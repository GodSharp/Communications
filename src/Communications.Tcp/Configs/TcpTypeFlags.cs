﻿namespace GodSharp.Communications.Tcp
{
    /// <summary>
    ///  The type of tcp socket protocol.
    /// </summary>
    public enum TcpTypeFlags
    {
        /// <summary>
        /// The client
        /// </summary>
        Client = 0,
        /// <summary>
        /// The server
        /// </summary>
        Server = 1
    }
}