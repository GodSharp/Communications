﻿namespace GodSharp.Communications.Tcp
{
    /// <summary>
    /// The type of socket config.
    /// </summary>
    public enum TcpConfigFlags
    {
        /// <summary>
        /// The client
        /// </summary>
        TcpClient = 0,
        /// <summary>
        /// The server
        /// </summary>
        TcpServer = 1,
        /// <summary>
        /// The server client
        /// </summary>
        TcpServerClient = 2
    }
}