﻿namespace GodSharp.Communications.Tcp
{
    /// <summary>
    ///  The config of tcp socket protocol.
    /// </summary>
    public class TcpConfig
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the host.
        /// </summary>
        /// <value>
        /// The host.
        /// </value>
        public string Host { get; set; }

        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        /// <value>
        /// The port.
        /// </value>
        public int Port { get; set; }

        /// <summary>
        /// Gets or sets the type of the socket.
        /// </summary>
        /// <value>
        /// The type of the socket.
        /// </value>
        public TcpConfigFlags ConfigType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="TcpConfig"/> is available.
        /// </summary>
        /// <value>
        ///   <c>true</c> if available; otherwise, <c>false</c>.
        /// </value>
        public bool Available { get; set; }
        
        /// <summary>
        /// Gets or sets the misc.
        /// </summary>
        /// <value>
        /// The misc.
        /// </value>
        public string Misc { get; set; }

        /// <summary>
        /// Gets or sets the entry point.
        /// </summary>
        /// <value>
        /// The entry point.
        /// </value>
        public string EntryPoint { get; set; }
    }
}
