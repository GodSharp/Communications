﻿using GodSharp.Communications.Abstractions;
using GodSharp.Sockets;

namespace GodSharp.Communications.Socket
{
    /// <summary>
    /// ITcpCommunicationProvider
    /// </summary>
    /// <seealso cref="GodSharp.Communications.Abstractions.ICommunicationProvider" />
    public interface ITcpCommunicationProvider: ICommunicationProvider
    {
        /// <summary>
        /// Gets the TCP connection.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        ITcpConnection GetTcpConnection(int id);
    }
}