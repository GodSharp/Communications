﻿using GodSharp.Communications.Abstractions;
using GodSharp.Sockets;

namespace GodSharp.Communications.Socket
{
    /// <summary>
    /// IUdpCommunicationProvider
    /// </summary>
    /// <seealso cref="GodSharp.Communications.Abstractions.ICommunicationProvider" />
    public interface IUdpCommunicationProvider : ICommunicationProvider
    {
        /// <summary>
        /// Gets the UDP connection.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IUdpConnection GetUdpConnection(int id);
    }
}
