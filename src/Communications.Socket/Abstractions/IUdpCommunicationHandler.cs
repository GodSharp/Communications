﻿using GodSharp.Communications.Abstractions;
using GodSharp.Sockets;

namespace GodSharp.Communications.Socket
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUdpCommunicationHandler : ICommunicationHandler
    {
        /// <summary>
        /// Called when [Udp data handler].
        /// </summary>
        /// <param name="destination">The destination.</param>
        /// <param name="buffer">The buffer.</param>
        /// <param name="sender">The sender.</param>
        void OnUdpDataHandler(string destination, byte[] buffer, IUdpConnection sender);
    }
}
