﻿using GodSharp.Communications.Abstractions;
using GodSharp.Sockets;

namespace GodSharp.Communications.Socket
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITcpCommunicationHandler : ICommunicationHandler
    {
        /// <summary>
        /// Called when [Tcp data handler].
        /// </summary>
        /// <param name="destination">The destination.</param>
        /// <param name="buffer">The buffer.</param>
        /// <param name="sender">The sender.</param>
        void OnTcpDataHandler(string destination, byte[] buffer, ITcpConnection sender);
    }
}
