﻿using GodSharp.Communications.Abstractions;
using System;

namespace GodSharp.Communications.Socket
{
    /// <summary>
    /// 
    /// </summary>
    public static class ICommunicationServiceExtension
    {
        /// <summary>
        /// Uses the TCP provider.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="optionsAction">The options action.</param>
        public static void UseTcpProvider(this ICommunicationManager service, Action<TcpCommunicationProviderOptions> optionsAction)
        {
            TcpCommunicationProviderOptions parameter = new TcpCommunicationProviderOptions();

            optionsAction?.Invoke(parameter);

            UseTcpProvider(service, parameter);
        }

        /// <summary>
        /// Uses the TCP provider.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="options">The options.</param>
        public static void UseTcpProvider(this ICommunicationManager service, TcpCommunicationProviderOptions options)
        {
            service.AddProvider(new TcpCommunicationProvider(options));
        }

        /// <summary>
        /// Uses the UDP provider.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="optionsAction">The options action.</param>
        public static void UseUdpProvider(this ICommunicationManager service, Action<UdpCommunicationProviderOptions> optionsAction)
        {
            UdpCommunicationProviderOptions parameter = new UdpCommunicationProviderOptions();

            optionsAction?.Invoke(parameter);

            UseUdpProvider(service, parameter);
        }

        /// <summary>
        /// Uses the UDP provider.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="options">The options.</param>
        public static void UseUdpProvider(this ICommunicationManager service, UdpCommunicationProviderOptions options)
        {
            service.AddProvider(new UdpCommunicationProvider(options));
        }
    }
}
