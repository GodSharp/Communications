﻿using GodSharp.Communications.Abstractions;
using GodSharp.Sockets;
using System.Collections.Generic;

namespace GodSharp.Communications.Socket
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="GodSharp.Communications.Abstractions.CommunicationProviderOptionsBase" />
    public class UdpCommunicationProviderOptions: CommunicationProviderOptionsBase
    {
        /// <summary>
        /// Gets or sets the configs.
        /// </summary>
        /// <value>
        /// The configs.
        /// </value>
        public ICollection<SocketConfig> Configs { get; set; }
        
        /// <summary>
        /// Gets or sets the exception handler.
        /// </summary>
        /// <value>
        /// The exception handler.
        /// </value>
        public EventHandler<NetClientEventArgs<IUdpConnection>> ExceptionHandler { get; set; }
    }
}