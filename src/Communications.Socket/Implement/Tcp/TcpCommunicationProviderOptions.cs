﻿using GodSharp.Communications.Abstractions;
using GodSharp.Sockets;
using System.Collections.Generic;

namespace GodSharp.Communications.Socket
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="GodSharp.Communications.Abstractions.CommunicationProviderOptionsBase" />
    public class TcpCommunicationProviderOptions: CommunicationProviderOptionsBase
    {
        /// <summary>
        /// Gets or sets the configs.
        /// </summary>
        /// <value>
        /// The configs.
        /// </value>
        public ICollection<SocketConfig> Configs { get; set; }
        
        /// <summary>
        /// Gets or sets the client connected handler.
        /// </summary>
        /// <value>
        /// The client connected handler.
        /// </value>
        public EventHandler<NetClientEventArgs<ITcpConnection>> OnClientConnectedHandler { get; set; }

        /// <summary>
        /// Gets or sets the client disconnected handler.
        /// </summary>
        /// <value>
        /// The client disconnected handler.
        /// </value>
        public EventHandler<NetClientEventArgs<ITcpConnection>> OnClientDisconnectedHandler { get; set; }
        
        /// <summary>
        /// Gets or sets the client connected handler.
        /// </summary>
        /// <value>
        /// The client connected handler.
        /// </value>
        public EventHandler<NetClientEventArgs<ITcpConnection>> ServerClientConnectedHandler { get; set; }

        /// <summary>
        /// Gets or sets the client disconnected handler.
        /// </summary>
        /// <value>
        /// The client disconnected handler.
        /// </value>
        public EventHandler<NetClientEventArgs<ITcpConnection>> OnServerClientDisconnectedHandler { get; set; }

        /// <summary>
        /// Gets or sets the on client exception.
        /// </summary>
        /// <value>
        /// The on client exception.
        /// </value>
        public EventHandler<NetClientEventArgs<ITcpConnection>> OnClientException { get; set; }

        /// <summary>
        /// Gets or sets the on server exception.
        /// </summary>
        /// <value>
        /// The on server exception.
        /// </value>
        public EventHandler<NetServerEventArgs> OnServerException { get; set; }
    }
}