﻿namespace GodSharp.Communications.Socket
{
    /// <summary>
    /// The type of socket config.
    /// </summary>
    public enum SocketConfigFlags
    {
        /// <summary>
        /// The client
        /// </summary>
        TcpClient = 0,
        /// <summary>
        /// The server
        /// </summary>
        TcpServer = 1,
        /// <summary>
        /// The server client
        /// </summary>
        TcpServerClient = 2,
        /// <summary>
        /// The UDP client
        /// </summary>
        UdpClient = 3
    }
}