﻿using GodSharp.Communications.Abstractions;

namespace GodSharp.Communications.Socket
{
    /// <summary>
    ///  The config of socket protocol.
    /// </summary>
    public class SocketConfig : ConfigBase
    {
        /// <summary>
        /// Gets or sets the host.
        /// </summary>
        /// <value>
        /// The host.
        /// </value>
        public string Host { get; set; }

        /// <summary>
        /// Gets or sets the remote port.
        /// </summary>
        /// <value>
        /// The remote port.
        /// </value>
        public int? RemotePort { get; set; }

        /// <summary>
        /// Gets or sets the local port.
        /// </summary>
        /// <value>
        /// The local port.
        /// </value>
        public int? LocalPort { get; set; }

        /// <summary>
        /// Gets or sets the type of the socket.
        /// </summary>
        /// <value>
        /// The type of the socket.
        /// </value>
        public SocketConfigFlags ConfigType { get; set; }
    }
}
