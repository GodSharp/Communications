﻿namespace GodSharp.Communications.Socket
{
    /// <summary>
    ///  The type of socket protocol.
    /// </summary>
    public enum SocketTypeFlags
    {
        /// <summary>
        /// The client
        /// </summary>
        TcpClient = 0,
        /// <summary>
        /// The server
        /// </summary>
        TcpServer = 1,
        /// <summary>
        /// The UDP client
        /// </summary>
        UdpClient = 2
    }
}