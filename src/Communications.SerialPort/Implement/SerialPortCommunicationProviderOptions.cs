﻿using GodSharp.Communications.Abstractions;
using System.Collections.Generic;

namespace GodSharp.Communications.SerialPort
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="GodSharp.Communications.Abstractions.CommunicationProviderOptionsBase" />
    public class SerialPortCommunicationProviderOptions: CommunicationProviderOptionsBase
    {
        /// <summary>
        /// Gets or sets the configs.
        /// </summary>
        /// <value>
        /// The configs.
        /// </value>
        public ICollection<SerialPortConfig> Configs { get; set; }
    }
}