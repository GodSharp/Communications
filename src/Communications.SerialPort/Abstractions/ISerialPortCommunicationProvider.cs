﻿using GodSharp.Communications.Abstractions;
using GodSharp.SerialPort;

namespace GodSharp.Communications.SerialPort
{
    /// <summary>
    /// ISerialPortCommunicationProvider
    /// </summary>
    /// <seealso cref="GodSharp.Communications.Abstractions.ICommunicationProvider" />
    public interface ISerialPortCommunicationProvider : ICommunicationProvider
    {
        /// <summary>
        /// Gets the SerialPort sender.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        GodSerialPort GetSerialPort(int id);
    }
}
