﻿using GodSharp.Communications.Abstractions;
using GodSharp.SerialPort;

namespace GodSharp.Communications.SerialPort
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISerialPortCommunicationHandler : ICommunicationHandler
    {
        /// <summary>
        /// Called when [SerialPort data handler].
        /// </summary>
        /// <param name="destination">The destination.</param>
        /// <param name="buffer">The buffer.</param>
        /// <param name="sender">The sender.</param>
        void OnSerialPortDataHandler(string destination, byte[] buffer, GodSerialPort sender);
    }
}
