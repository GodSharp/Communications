﻿using GodSharp.Communications.Abstractions;

namespace GodSharp.Communications.SerialPort
{
    /// <summary>
    ///  The config of serial port protocol.
    /// </summary>
    public class SerialPortConfig : ConfigBase
    {
        /// <summary>
        /// Gets or sets the name of the device.
        /// </summary>
        /// <value>
        /// The name of the device.
        /// </value>
        public string DeviceName { get; set; }

        /// <summary>
        /// Gets or sets the name of the port.
        /// </summary>
        /// <value>
        /// The name of the port.
        /// </value>
        public string PortName { get; set; }

        /// <summary>
        /// Gets or sets the baud rate.
        /// </summary>
        /// <value>
        /// The baud rate.
        /// </value>
        public int BaudRate { get; set; } = 9600;

        /// <summary>
        /// Gets or sets the data bits.
        /// </summary>
        /// <value>
        /// The data bits.
        /// </value>
        public int DataBits { get; set; } = 8;

        /// <summary>
        /// Gets or sets the stop bits.
        /// </summary>
        /// <value>
        /// The stop bits.
        /// </value>
        public int StopBits { get; set; }

        /// <summary>
        /// Gets or sets the parity bits.
        /// </summary>
        /// <value>
        /// The parity bits.
        /// </value>
        public string ParityBits { get; set; }

        /// <summary>
        /// Gets or sets the handshakes.
        /// </summary>
        /// <value>
        /// The handshakes.
        /// </value>
        public string Handshakes{ get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerialPortConfig"/> class.
        /// </summary>
        public SerialPortConfig()
        {
        }
    }
}
