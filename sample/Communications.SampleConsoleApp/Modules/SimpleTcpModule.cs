﻿using GodSharp.Communications.Abstractions;
using GodSharp.Communications.Tcp;
using GodSharp.Sockets;

namespace Communications.SampleConsoleApp.Modules
{
    [EntryPoint("OCV1")]
    [EntryPoint("OCV2")]
    public class SimpleTcpModule : CommunicationModuleBase, ICommunicationTcpHandler
    {
        public void OnTcpDataHandler(string destination, byte[] buffer, TcpSender sender)
        {

        }
    }
}
