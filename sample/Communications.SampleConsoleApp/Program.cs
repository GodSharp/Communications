﻿using GodSharp.Communications;
using GodSharp.Communications.Abstractions;
using GodSharp.Communications.Tcp;
using System;

namespace Communications.SampleConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            ICommunicationService service = new CommunicationService();

            service.AddTcpProvider(x => { x.ServerClientConfigs = null; });

            service.Builder();
        }
    }
}
