﻿using GodSharp.Communications.Abstractions;
using GodSharp.Communications.OpcDa;
using GodSharp.Opc;
using System;

namespace Communications.OpcDa.Sample
{
    [EntryPoint("EntryPoint100")]
    [EntryPoint("EntryPoint122")]
    public class OpcDaModule122 : CommunicationModuleBase, IOpcDaCommunicationHandler
    {
        public void OnOpcDaDataHandler(string destination, OpcTagItem item, OpcDaClient client)
        {
            Console.WriteLine($"ModuleName: {this.GetType().Name}");
            Console.WriteLine($"EntryPoint: {destination}, ItemId: {item.ItemId }, Value: {item.Value }");
            Console.WriteLine("----------------------------------------------");
        }
    }
}
