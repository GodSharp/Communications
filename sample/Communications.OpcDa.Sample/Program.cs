﻿using GodSharp.Communications;
using GodSharp.Communications.Abstractions;
using GodSharp.Communications.OpcDa;
using GodSharp.Opc;
using System;
using System.Collections.Generic;

namespace Communications.OpcDa.Sample
{
    class Program
    {
        static ICommunicationManager communication = null;

        static void Main(string[] args)
        {
            Console.WriteLine("OpcDa Provider Sample for Communications!");

            communication = new CommunicationManager();

            communication.UseOpcDaProvider(new OpcDaCommunicationProviderOptions()
            {
                Clients = new List<OpcDaClientConfig>()
                {
                    new OpcDaClientConfig(new OpcDaServerConfig("KEPware.KEPServerEx.V4",updateRate:50){ },new List<OpcDaTagConfig>()
                    {
                        new OpcDaTagConfig(11001,"Channel1.Device1.Tag1"){ EntryPoint="EntryPoint100" },
                        new OpcDaTagConfig(11002,"Channel1.Device1.Tag2"){ EntryPoint="EntryPoint100" },
                        new OpcDaTagConfig(11003,"Channel1.Device1.Tag3"){ EntryPoint="EntryPoint100" },
                        new OpcDaTagConfig(11101,"Channel1.Device1.Group1.Tag1"){ EntryPoint="EntryPoint111" },
                        new OpcDaTagConfig(11102,"Channel1.Device1.Group1.Tag2"){ EntryPoint="EntryPoint111" },
                        new OpcDaTagConfig(11103,"Channel1.Device1.Group1.Tag3"){ EntryPoint="EntryPoint111" },
                        new OpcDaTagConfig(11111,"Channel1.Device1.Group1.SubGroup1.Tag1"){ EntryPoint="EntryPoint111" },
                        new OpcDaTagConfig(11112,"Channel1.Device1.Group1.SubGroup1.Tag2"){ EntryPoint="EntryPoint111" },
                        new OpcDaTagConfig(11113,"Channel1.Device1.Group1.SubGroup1.Tag3"){ EntryPoint="EntryPoint111" },
                        new OpcDaTagConfig(11201,"Channel1.Device1.Group2.Tag1"){ EntryPoint="EntryPoint122" },
                        new OpcDaTagConfig(11202,"Channel1.Device1.Group2.Tag2"){ EntryPoint="EntryPoint122" },
                        new OpcDaTagConfig(11203,"Channel1.Device1.Group2.Tag3"){ EntryPoint="EntryPoint122" },
                        new OpcDaTagConfig(11221,"Channel1.Device1.Group2.SubGroup2.Tag1"){ EntryPoint="EntryPoint122" },
                        new OpcDaTagConfig(11222,"Channel1.Device1.Group2.SubGroup2.Tag2"){ EntryPoint="EntryPoint122" },
                        new OpcDaTagConfig(11223,"Channel1.Device1.Group2.SubGroup2.Tag3"){ EntryPoint="EntryPoint122" }
                    }){ Id=10000}
                },
                ShutdownEventHandler = OpcDaShutdownEventHandler
            });

            communication.RegisterContainer(new ModuleContainer());

            communication = communication.Builder();

            Console.WriteLine("CommunicationService Starting...");

            communication.Start();

            Console.WriteLine("CommunicationService Started!");

            Console.ReadLine();
            Console.ReadLine();
            Console.ReadLine();
            Console.ReadLine();
            Console.ReadLine();

            communication.Stop();
        }

        static void OpcDaShutdownEventHandler(string reason,OpcDaClient client)
        {
            Console.WriteLine($"Opc Server [{client.Option.ProgId}] Shutdown!");
        }
    }
}
