﻿using GodSharp.Communications.Abstractions;
using GodSharp.Communications.Socket;
using GodSharp.Sockets;
using System;
using System.Text;

namespace Communications.Tcp.Sample.Modules.Clients
{
    [EntryPoint("TcpClient1")]
    public class TcpClientModule1 : CommunicationModuleBase, ITcpCommunicationHandler
    {
        public void OnTcpDataHandler(string destination, byte[] buffer, ITcpConnection sender)
        {
            Console.WriteLine($"ModuleName: {this.GetType().Name}");
            Console.WriteLine($"EntryPoint: {destination}, RemoteEndPoint: {sender.RemoteEndPoint }, Data: {Encoding.UTF8.GetString(buffer) }");
            Console.WriteLine("----------------------------------------------");
        }
    }
}