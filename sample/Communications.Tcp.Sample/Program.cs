﻿using GodSharp.Communications;
using GodSharp.Communications.Abstractions;
using GodSharp.Communications.Socket;
using GodSharp.Sockets;
using System;

namespace Communications.Tcp.Sample
{
    class Program
    {
        static ICommunicationManager communication = null;

        static void Main(string[] args)
        {
            Console.WriteLine("OpcDa Provider Sample for Communications!");

            communication = new CommunicationManager();

            communication.UseTcpProvider(new TcpCommunicationProviderOptions()
            {
                Configs=new SocketConfig[]
                {
                    new SocketConfig(){ Id=12001, EntryPoint="TcpClient1", ConfigType= SocketConfigFlags.TcpClient, Host="127.0.0.1", RemotePort=7788 ,Available=true},
                    new SocketConfig(){ Id=12002, EntryPoint="TcpClient2", ConfigType= SocketConfigFlags.TcpClient, Host="127.0.0.1", RemotePort=7799,Available=true },
                    new SocketConfig(){ Id=12003, EntryPoint="TcpServer1", ConfigType= SocketConfigFlags.TcpServer, Host="127.0.0.1", LocalPort=8899 ,Available=true},
                    new SocketConfig(){ Id=12004, EntryPoint="TcpServer2", ConfigType= SocketConfigFlags.TcpServer, Host="127.0.0.1", LocalPort=9988,Available=true },
                    new SocketConfig(){ Id=12005, EntryPoint="TcpServer1", ConfigType= SocketConfigFlags.TcpServerClient, Host="127.0.0.1", RemotePort=12005,Available=true },
                    new SocketConfig(){ Id=12006, EntryPoint="TcpServer1", ConfigType= SocketConfigFlags.TcpServerClient, Host="127.0.0.1", RemotePort=12006,Available=true },
                    new SocketConfig(){ Id=12007, EntryPoint="TcpServer2", ConfigType= SocketConfigFlags.TcpServerClient, Host="127.0.0.1", RemotePort=12007,Available=true },
                    new SocketConfig(){ Id=12008, EntryPoint="TcpServer2", ConfigType= SocketConfigFlags.TcpServerClient, Host="127.0.0.1", RemotePort=12008,Available=true }
                },
                OnClientException = OnClientExceptionEventHandler
            });

            communication.RegisterContainer(new ModuleContainer());

            communication = communication.Builder();

            Console.WriteLine("CommunicationService Starting...");

            communication.Start();

            Console.WriteLine("CommunicationService Started!");

            Console.ReadLine();

            communication.Stop();
        }

        static void OnClientExceptionEventHandler(NetClientEventArgs<ITcpConnection> args)
        {

        }
    }
}
